/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
		function getBasicInfo(){
			let fullName = prompt("What is your Full Name?");
			let currentAge = prompt("How old are you?");
			let currentAddress = prompt("Where do you live?");
			
			console.log("Hello, " + fullName);
			console.log("You are " + currentAge + " years old.");
			console.log("You live in " + currentAddress);
		}

		getBasicInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.


	
*/

	//second function here:
		function getFaveBands(){
			console.log("1. Parokya Ni Edgar");
			console.log("2. Kamikazee");
			console.log("3. Silent Sanctuary");
			console.log("4. RiverMaya");
			console.log("5. Eraserheads");
		}

		getFaveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.


	
*/
	
	//third function here:
		function ratingFaveMovies(){
			console.log("1. The Perks of Being a Wallflower")
			console.log("Rotten Tomatoes Rating: 89%")
			console.log("2. About Time ")
			console.log("Rotten Tomatoes Rating: 81%")
			console.log("3. Paper Towns ")
			console.log("Rotten Tomatoes Rating: 47%")
			console.log("4. La la land")
			console.log("Rotten Tomatoes Rating: 81%")
			console.log("5. The Greatest Showman")
			console.log("Rotten Tomatoes Rating: 86%")
		}

		ratingFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();